var service = {};

// count2 is a global variable used to see if we have synchronized before
var count2 = 0;

// oldDate is a global variable used to hold the oldest day synchronized from
var oldDate = 0;

/*
 * Date.prototype.getWeek
 * Adds a function to the Date.prototype that returns the current week number
 */
Date.prototype.getWeek = function() {
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    var dayNum = d.getUTCDay() || 7;
    d.setUTCDate(d.getUTCDate() + 4 - dayNum);
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
};

/*
 * service.startSyncPrintjobs
 * The function synchronizes the ElasticSearch index "printjobs" with the printjobs from the database.
 * On the initial run it gets data from a month and a week back, then increments count.
 * Then when this function is called again it syncs from the end of last sync to now
 *
 * @param element - takes a collection, that gets synchronized to Elasticsearch
 */




service.startSyncPrintjobs = function (element) {
    var stream;
    var count = 0;
    var d = new Date();
    var newDate = d.setMonth(new Date().getMonth()-1) - 604800000;
    if(count2 == 0) {
        stream = element.synchronize({'$and': [{'createdAt': {'$gte': newDate}}, {'createdAt': {'$lte': new Date().getTime()}}]});
        oldDate = new Date().getTime();
        count2++;
    } else {
        stream = element.synchronize({'$and': [{'createdAt': {'$gte': oldDate}}, {'createdAt': {'$lte': new Date().getTime()}}]});
        oldDate = new Date().getTime();
    }

    stream.on('data', function(err, doc){
        count++;

        console.log(count);
    });
    stream.on('close', function(){
        console.log('indexed ' + count + ' documents!');

    });
    stream.on('error', function(err){
        console.log(err);
    });

};

/*
 * service.startSyncLocations
 * This function synchronizes the Elasticsearch index "printers_with_locations" with data from the database
 * It creates the mapping for our "printers_with_locations"
 * @param element - takes a collection, that gets synchronized to Elasticsearch
 */
service.startSyncLocations = function (element) {
    var stream;
    var count = 0;

    element.createMapping(function(err, mapping) {
        if (err) {
            console.log('error creating mapping (you can safely ignore this)');
            console.log(err);
        } else {
            console.log('mapping created!');
            console.log(mapping);
        }
    });
    stream = element.synchronize();


    stream.on('data', function(err, doc){
        count++;
        console.log(count);
    });
    stream.on('close', function(){
        console.log('indexed ' + count + ' documents!');

    });
    stream.on('error', function(err){
        console.log(err);
    });
};

/*
 * service.searchAll searches through the chosen Elasticsearch index
 * and takes all JSON objects found and pushes them to into the matches array.
 *
 * @param element - takes a collection to search through
 */
service.searchAll  = function (element) {
    var matches = [];
    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},{"from": 0, "size":500},
            function(err, results) {
                for(var i in results['hits']){
                    for(var j in results['hits'][i]){
                        matches.push(results['hits'][i][j]['_source']);
                    }
                }
                if (matches.length > 0) {
                    resolve(matches);
                }
                else {
                    reject(Error("It broke"));
                }
            });


    });

};

/*
 * service.sortSearchWeekly takes an array of elements and sorts them by day
 *
 * @params element - takes an array to sort by days
 */
service.sortSearchWeekly = function (element) {

    var weekdays = [];
    var start = 0;
    var d = new Date();
    for(var i = 0; i < element.length; i++){

        if(d.getDay() != 0) {
            if (i > d.getDay() - 1) {
                weekdays.push(element[i]);

            }
            if (i == d.getDay() - 1) {
                start = i;
            }
        } else {
            if (i == 6) {
                start = i;
            }
        }
    }

    for(var i = 0; i <= start; i++){
        weekdays.push(element[i]);
    }

    return weekdays;

};
/*
 * service.searchWeeklyCharts searches through the Elasticsearch index between two dates, and takes all printjobs found and pushes
 * into an array of 7, one for each day.
 * It checks which day it is and pushes it the the correct spot in the array, idex 0 is monday and so forth.
 * It then sorts the array so it goes from 6 days ago before yesterday, till yesterday.
 * @params element - takes a collection of printjobs
 * @params printers - takes an array of printers
 */
service.searchWeeklyCharts = function (element, printers) {
    var d = new Date().getTime();
    var beforeD = new Date().getTime()-691200000;


    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all":{}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from" : beforeD, "to" : d}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "printer_per_day": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "day"

                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            },
            function(err, results) {
                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];
                var sorted = [];


                for(var i in aggs) {

                    var map = [0, 0, 0, 0, 0, 0, 0];
                    for (var j in aggs[i]['printer_per_day']['buckets']) {
                        var oldDay = new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getDay();
                        var newDay = new Date().getDay() - 1;
                        if(newDay == -1){
                            newDay = 6;
                        }

                        if (new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getWeek() == new Date().getWeek()) {


                            for (var k in map) {
                                if (oldDay == newDay && oldDay == k) {

                                    map[newDay] += aggs[i]['printer_per_day']['buckets'][j]['doc_count'];
                                }
                                else if (oldDay == k && oldDay != newDay && k < newDay) {
                                    map[k] += aggs[i]['printer_per_day']['buckets'][j]['doc_count'];
                                }
                            }
                        }

                        if (new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getWeek() == new Date().getWeek() - 1) {
                            for (var k in map) {



                                if (oldDay == k && (oldDay != newDay || oldDay == 0) && (k > newDay || k == 0)) {

                                    map[k] += aggs[i]['printer_per_day']['buckets'][j]['doc_count'];


                                }
                            }


                        }

                    }
                    sorted.push({printerId: aggs[i].key, data: service.sortSearchWeekly(map)});
                }

                if (sorted.length > 0) {
                    var res = [];


                    for (var i in printers) {
                        for (var j in sorted) {

                            if (printers[i].printerId == sorted[j].printerId) {
                                res.push({printerId: printers[i].printerId, data: sorted[j].data})

                            }

                        }
                    }
                    resolve(res);
                }
                else {
                    reject(Error("It broke"));
                }



            });

    });



};

/*
 * service.searchWeeklyAvg searches the Elasticsearch index for printjobs from one month and a week back up till now.
 * it uses the variable beforeOneWeek to make sure the average does not take values from the current week.
 * It then calculates the average for each printer on each day and push that to an array.
 * @param element - takes a collection of printjobs
 * @param printers - takes an array of printers
 */

service.searchWeeklyAvg = function (element, printers) {
    var other = [];
    var sorted = [];
    var beforeOneWeek = new Date(new Date().getTime() - (60 * 60 * 24 * 7 * 1000));
    var day = beforeOneWeek.getDay();
    var diffToMonday = beforeOneWeek.getDate() - day + (day === 0 ? -6 : 1);
    var lastMonday = new Date(beforeOneWeek.setDate(diffToMonday));
    var lastSunday = new Date(beforeOneWeek.setDate(diffToMonday + 6));

    lastSunday.setHours(23);
    lastSunday.setMinutes(59);
    var old = lastMonday.getTime() - (604800000*4);
    var newOld = new Date(old);
    newOld.setHours(2);
    newOld.setMinutes(00)
    var newDate = lastSunday.getTime() - 604800000;


    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": newOld.getTime(), "to": newDate}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "printer_per_day": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "day"
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            },
            function(err, results) {

                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];
                for(var i in aggs) {
                    var map = [0, 0, 0, 0, 0, 0, 0];

                    for (var j in aggs[i]['printer_per_day']['buckets']) {
                        var day = new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getDay();

                        for (var k in map) {
                            if (day == k) {
                                map[k] += aggs[i]['printer_per_day']['buckets'][j]['doc_count'];
                            }
                        }

                    }

                    for (var j in map) {
                        var avg = map[j] / 4;
                        other.push(avg);
                    }


                    sorted.push({printerId: aggs[i].key, data: service.sortSearchWeekly(other)});
                    other = [];

                }



                if (sorted.length > 0) {
                    var res = [];


                    for (var i in printers) {
                        for (var j in sorted) {

                            if (printers[i].printerId == sorted[j].printerId) {
                                res.push({printerId: printers[i].printerId, data: sorted[j].data})


                            }

                        }
                    }

                    resolve(res);
                }
                else {
                    reject(Error("It broke"));
                }
            });


    });

};

/*
 * service.getPrintersWeekly is a function that returns a list of printers with a increase or decrease in printjobs
 * bigger than its standard deviation * 1.5. It starts of with getting data from last monday + 4 weeks up till now.
 * It takes each printjob for each printer and calculates its growth, and then uses the variance formula to get the variance.
 * Then sorts the data from with the printer with higest growth first. Then it pushes the first 25 into a new array.
 * then it looks for the 25 with the biggest negative growth and pushes those.
 * @param element - takes a collection of printjobs
 * @params element2 - takes an array of printers
 */
service.getPrintersWeekly = function (element, printers) {
    var other = [];
    var n = [];
    var printer = [];
    var count = 0;
    var sum = 0;
    var beforeOneWeek = new Date(new Date().getTime() - 60 * 60 * 24 * 7 * 1000)
        , day = beforeOneWeek.getDay()
        , diffToMonday = beforeOneWeek.getDate() - day + (day === 0 ? -6 : 1)
        , lastMonday = new Date(beforeOneWeek.setDate(diffToMonday));


    var newDate = new Date().getTime();
    var oldDate = new Date();
    var old = lastMonday.getTime() - (604800000*4);
    var countSize = 0;
    var newOld = new Date(old);
    newOld.setHours(2);
    newOld.setMinutes(00)


    var myDay = new Date().getDay();

    if(myDay == 0){
        myDay = 6;
    } else {
        myDay = new Date().getDay() - 1;
    }

    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": newOld.getTime(), "to": newDate}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "print_per_day": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "day"
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            },
            function(err, results) {

                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];



                for(var i in aggs){



                    for (var j in aggs[i]['print_per_day']['buckets']) {
                        if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() < new Date().getWeek() - 1) {

                            sum += aggs[i]['print_per_day']['buckets'][j]['doc_count'];


                        }
                    }


                    growth = sum / 28;

                    n.push(growth);

                    sum = 0;
                    count = 0;

                }

                var x = 0;
                var sumDay = 0;

                for(var i in aggs){

                    for (var j in aggs[i]['print_per_day']['buckets']) {
                        if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() < new Date().getWeek() - 1) {
                            x += Math.pow(aggs[i]['print_per_day']['buckets'][j]['doc_count'] - n[i], 2)
                        }
                        if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() == new Date().getWeek()) {
                            if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() <= myDay ) {
                                sumDay += aggs[i]['print_per_day']['buckets'][j]['doc_count'];
                            }
                        } else if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() == new Date().getWeek() - 1) {
                            if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() > myDay || new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() == 0) {
                                sumDay += aggs[i]['print_per_day']['buckets'][j]['doc_count'];

                            }

                        }

                    }

                    var test = 0;

                    if (x != 0) {

                        var o = x / 3

                        test = Math.sqrt(o).toFixed(2);
                    }

                    var k = test * 1.5;




                    if (n[i] != 0 && (n[i]) > 3) {



                        if (sumDay >= (n[i]*7) + k) {


                            var growth = sumDay - (n[i]*7);


                            printer.push({printerId: aggs[i].key, growth: growth.toFixed(2), avg: (n[i]*7).toFixed(0)});


                        } else if (sumDay <= (n[i]*7) - k) {

                            var growth = sumDay - (n[i]*7);


                            printer.push({printerId: aggs[i].key, growth: growth.toFixed(2), avg: (n[i]*7).toFixed(0)});

                        }
                    }

                    x = 0;
                    sumDay = 0;

                }


                printer.sort(function (a,b) {
                    if(b.growth < 0 && a.growth < 0) {
                        return a.growth - b.growth;
                    } else {
                        return b.growth - a.growth;
                    }
                });


                for(var i in printer) {

                    if(printer[i].growth > 0) {
                        if (other.length <= 24) {
                            other.push({printerId: printer[i].printerId, growth: printer[i].growth, avg: printer[i].avg})
                            countSize++;
                        }
                    } else {
                        if (other.length <= 24+countSize && printer[i].growth < 0) {
                            other.push({printerId: printer[i].printerId, growth: printer[i].growth, avg: printer[i].avg})
                        }
                    }
                }


                var m = [];
                var matches = [];

                if (other.length > 0) {
                    m = printers;
                    for(var j in other) {
                        for(var p in m){
                            if (m[p].printerId === other[j].printerId) {
                                matches.push({printerId: m[p].printerId, hostId: m[p].hostId, locationId: m[p].locationId, growth: other[j].growth, avg: other[j].avg});
                            }
                        }
                    }


                    resolve(matches);
                }
                else {
                    reject(Error("It broke"));
                }
            });


    });


};

/*
 * searchDailyCharts finds all the charts needed to show on the website.
 * We use Elasticsearch to search after charts from yesterday at 12 am to 11:59pm
 * We then sort every printers printjob into showing how many printjobs they had every two hours.
 *
 * @param element - Takes a collection so Elasticsearch can search through it.
 *
 * @param printers, is used for sorting the right charts needed for the website.
 * the function getPrintersHourly finds the correct printers needed.
 * We don't need all the printers, they need to have a certain amount of printjobs,
 * and thats for getPrintersDaily to decide.
 *
 * OBS: We calculate from yesterday's data.
 *
 */

service.searchDailyCharts = function (element, printers) {

    var sorted = [];
    var day;
    var night;
    var today = new Date();
    if(today.getDate() != 1) {
        day = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1, 0, 1, 0);
        night = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1, 23, 59, 0);
    } else {
        day = new Date(today.getFullYear(), today.getMonth(), 0, 0, 1, 0);
        night = new Date(today.getFullYear(), today.getMonth(), 0, 23, 59, 0);

    }


    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from" : day.getTime(), "to" : night.getTime()}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "print_hour": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "2h"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            function(err, results) {

                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];

                var hours = [6.00, 8.00, 10.00, 12.00, 14.00, 16.00, 18.00, 20.00, 22.00];



                for(var i in aggs) {
                    var map = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                    for (var j in aggs[i]['print_hour']['buckets']) {

                        var day = new Date(aggs[i]['print_hour']['buckets'][j]['key']);
                        var time = day.getHours();


                        for (var k = 0; k < hours.length; k++) {
                            if (time >= hours[k] && time < (hours[k]) + 2.00) {
                                map[k] += aggs[i]['print_hour']['buckets'][j]['doc_count'];
                            }



                        }
                    }

                    sorted.push({printerId: aggs[i]['key'], data: map});
                }



                if (sorted.length > 0) {
                    var res = [];
                    for (var i in printers) {
                        for (var j in sorted) {
                            if (printers[i].printerId == sorted[j].printerId) {
                                res.push({printerId: printers[i].printerId, data: sorted[j].data})

                            }

                        }
                    }

                    resolve(res);
                }
                else {
                    reject(Error("It broke"));
                }

            });


    });

}

/*
 * searchDailyAvg is used for finding the average of every printer.
 * searchDailyAvg makes the line on the chart, that shows a printers average pr. day.
 * To find the average, we go a month back and look at the last 4 weeks.
 *
 */

service.searchDailyAvg = function (element, printers) {
    var sorted = [];
    var today = new Date();
    var newDate;
    var newDay = 0;

    if(today.getDate() != 1) {
        newDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    } else {
        newDate = new Date(today.getFullYear(), today.getMonth(), 0);
    }
    if(new Date().getDay() == 0){
        newDay = 6;
    } else {
        newDay = new Date().getDay() - 1;
    }

    var oldDate = new Date().setMonth(new Date().getMonth() - 1);

    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": oldDate, "to": newDate.getTime()}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "printer_per_day": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "day",
                                        },
                                        "aggs": {
                                            "print_per_hour": {
                                                "date_histogram": {
                                                    "field": "createdAt",
                                                    "interval": "2h"
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            },
            function(err, results) {

                var tmap = {};

                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];


                var hours = [6.00, 8.00, 10.00, 12.00, 14.00, 16.00, 18.00, 20.00, 22.00];


                for(var i in aggs) {

                    var map = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                    for (var j in aggs[i]['printer_per_day']['buckets']) {
                        if (new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getDay() == newDay) {
                            for (var k in aggs[i]['printer_per_day']['buckets'][j]['print_per_hour']['buckets']) {
                                var day = new Date(aggs[i]['printer_per_day']['buckets'][j]['print_per_hour']['buckets'][k]['key']);
                                var time = day.getHours();

                                for (var c = 0; c < hours.length; c++) {
                                    if (time >= hours[c] && time < (hours[c]) + 2.00) {
                                        map[c] += aggs[i]['printer_per_day']['buckets'][j]['print_per_hour']['buckets'][k]['doc_count'];
                                    }


                                }

                            }


                        }

                        tmap[aggs[i].key] = map;


                    }

                    for(var j in tmap[aggs[i].key]){

                        tmap[aggs[i].key][j] = (tmap[aggs[i].key][j] / 4);
                    }

                    sorted.push({printerId: aggs[i].key, data: tmap[aggs[i].key]})

                }


                if (sorted.length > 0) {
                    var res = [];
                    for (var i in printers) {
                        for (var j in sorted) {

                            if (printers[i].printerId == sorted[j].printerId) {
                                res.push({printerId: printers[i].printerId, data: sorted[j].data})

                            }

                        }
                    }
                    resolve(res);
                }
                else {
                    reject(Error("It broke"));
                }
            });


    });

};

/*
 * getPrintersDaily is a function used to make the table consisting of printers on the daily tab, on the right.
 * The table shows either if the printer prints less or prints more today, than usual.
 * In this function, we calculate the average, and then calculate variance and standard deviation to avoid
 * smaller printers dominating the table of printers, because if a small printer on average prints 1 pr. day, then printing 2
 * can cause a growth of 100%.
 * So before a printer can be on the table, it must have printjobs*2 of its standard deviation.
 * Here we use Elasticsearch to find all printjobs from last month till today, so we can calculate all the
 * things mentioned above.
 *
 * getPrintersDaily returns printers, that is used in methods lke searchDailyAvg and searchDailyCharts, so they
 * can return the right data to show on the chart.
 *
 * @param element - Takes a collection that Elasticsearch searches through.
 * @param printers - Takes an array of printers to get the correct information about the printer, like hostId and locationId.
 *
 * OBS: We calculate from yesterday's data.
 *
 */

service.getPrintersDaily = function (element, printers) {

    var other = [];
    var n = [];
    var printer = [];
    var count = 0;
    var sum = 0;
    var today = new Date();
    var newDate;
    var countSize = 0;
    var newDay = 0;


    if(today.getDate() != 1) {
        newDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    } else {
        newDate = new Date(today.getFullYear(), today.getMonth(), 0);
    }

    var oldDate = new Date().setMonth(new Date().getMonth() - 1);
    newDate.setHours(2);
    newDate.setMinutes(0);


    if(new Date().getDay() == 0){
        newDay = 6;
    } else {
        newDay = new Date().getDay() - 1;
    }

    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": oldDate, "to": newDate.getTime()}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "print_per_day": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "day"

                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            },
            function(err, results) {

                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];

                for(var i in aggs){

                    for (var j in aggs[i]['print_per_day']['buckets']) {

                        if(new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() < new Date().getWeek() - 1) {
                            if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() == new Date().getDay() - 1) {
                                sum += aggs[i]['print_per_day']['buckets'][j]['doc_count'];
                            }
                        }

                    }

                    growth = sum / 4;

                    n.push(growth);
                    sum = 0;
                    count = 0;

                }

                var x = 0;
                var dayData = 0;

                for(var i in aggs){
                    for (var j in aggs[i]['print_per_day']['buckets']) {

                        if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() < new Date().getWeek() - 1 ) {

                            if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() == newDay) {
                                x += Math.pow(aggs[i]['print_per_day']['buckets'][j]['doc_count'] - n[i], 2);

                            }
                        }

                        if (newDay != 0) {
                            if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() == new Date().getWeek()) {
                                if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() == newDay) {
                                    dayData = aggs[i]['print_per_day']['buckets'][j]['doc_count'];

                                }
                            }
                        } else {
                            if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getWeek() == new Date().getWeek() - 1) {

                                if (new Date(aggs[i]['print_per_day']['buckets'][j]['key']).getDay() == newDay) {

                                    dayData = aggs[i]['print_per_day']['buckets'][j]['doc_count'];

                                }
                            }
                        }

                    }

                    var test = 0;

                    if(x != 0) {
                        test = Math.sqrt((x / (4 - 1))).toFixed(2);
                    }
                    var k = test * 2;
                    if(n[i] != 0 && (n[i]) > 3) {
                        if (dayData >= (n[i] + k)) {
                            var growth = dayData - n[i];
                            printer.push({printerId: aggs[i].key, growth: growth.toFixed(0), avg: n[i].toFixed(0)});


                        } else if (dayData  <= n[i] - k) {
                            var growth = dayData - n[i];
                            printer.push({printerId: aggs[i].key, growth: growth.toFixed(0), avg: n[i].toFixed(0)});
                        }
                    }
                    x = 0;
                    dayData = 0;

                }

                printer.sort(function (a,b) {
                    if(b.growth < 0 && a.growth < 0) {
                        return a.growth - b.growth;
                    } else {
                        return b.growth - a.growth;
                    }
                });



                for(var i in printer) {
                    if(printer[i].growth > 0) {
                        if (other.length <= 24) {
                            other.push({printerId: printer[i].printerId, growth: printer[i].growth, avg: printer[i].avg})
                            countSize++;
                        }
                    } else {
                        if (other.length <= 24+countSize && printer[i].growth < 0) {
                            other.push({printerId: printer[i].printerId, growth: printer[i].growth, avg: printer[i].avg})
                        }
                    }
                }



                var m = [];
                var matches = [];

                if (other.length > 0) {
                    m = printers;
                    for(var j in other) {
                        for(var p in m){
                            if (m[p].printerId === other[j].printerId) {
                                matches.push({printerId: m[p].printerId, hostId: m[p].hostId, locationId: m[p].locationId, growth: other[j].growth, avg: other[j].avg});
                            }
                        }
                    }
                    resolve(matches);
                }
                else {
                    reject(Error("It broke"));
                }
            });

    });


}

/*
 * searchHourlyCharts finds all the charts needed to show on the website.
 * We use Elasticsearch to search after charts from today at 12 am to 11:59pm
 * We then sort every printers printjob into showing how many printjobs they had every hour.
 *
 * @param element - Takes a collection, that Elasticsearch searches through.
 * @param printers, is used for sorting the right charts needed for the website.
 *
 * the function getPrintersHourly finds the correct printers needed.
 * We don't need all the printers, they need to have a certain amount of printjobs,
 * and thats for getPrintersHourly to decide.
 *
 */
service.searchHourlyCharts = function (element, printers) {
    var sorted = [];
    var today = new Date();
    var day = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 1, 0);
    var night = new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours(), today.getMinutes());


    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": day.getTime(), "to": night.getTime()}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "print_hour": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "1h"

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            function(err, results) {
                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];

                var hours = [6.00, 7.00, 8.00, 9.00, 10.00, 11.00, 12.00, 13.00, 14.00, 15.00, 16.00, 17.00, 18.00];

                for(var i in aggs) {
                    var map = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    for (var j in aggs[i]['print_hour']['buckets']) {
                        var day = new Date(aggs[i]['print_hour']['buckets'][j]['key']);
                        var time = day.getHours();

                        for (var k = 0; k < hours.length; k++) {
                            if (time >= hours[k] && time < (hours[k]) + 1.00) {
                                map[k] += aggs[i]['print_hour']['buckets'][j]['doc_count'];
                            }

                        }
                    }

                    sorted.push({printerId: aggs[i]['key'], data: map});
                }

                if (sorted.length > 0) {
                    var res = [];

                    for (var i in printers) {
                        for (var j in sorted) {

                            if (printers[i].printerId == sorted[j].printerId) {
                                res.push({printerId: printers[i].printerId, data: sorted[j].data})

                            }

                        }
                    }
                    resolve(res);
                }
                else {
                    reject(Error("It broke"));
                }

            });
    });

}
/*
 * searchHourlyAvg is used for finding the average of every printer.
 * searchHourlyAvg makes the line on the chart, that shows a printers average pr. day.
 * To find the average, we go a month back and look at the last 4 weeks.
 *
 * @param printers, is used for sorting the right charts needed for the website.
 * the function getPrintersHourly finds the correct printers needed.
 * We don't need all the printers, they need to have a certain amount of printjobs,
 * and thats for getPrintersHourly to decide.
 * @param element - Takes a collection. We then use Elasticsearch to search to search through the collection.
 */
service.searchHourlyAvg = function (element, printers) {
    var sorted = [];
    var newDate = new Date().getTime();
    var oldDate = new Date().setMonth(new Date().getMonth() - 1);

    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": oldDate, 'to': newDate}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "printer_per_day": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "day",

                                        },
                                        "aggs": {
                                            "print_per_hour": {
                                                "date_histogram": {
                                                    "field": "createdAt",
                                                    "interval": "1h"
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            },
            function(err, results) {
                var tmap = {};

                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];
                var hours = [6.00, 7.00, 8.00, 9.00, 10.00, 11.00, 12.00, 13.00, 14.00, 15.00, 16.00, 17.00, 18.00];

                for(var i in aggs) {

                    var map = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                    for (var j in aggs[i]['printer_per_day']['buckets']) {

                        if(new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getWeek() != new Date().getWeek()) {

                            if (new Date(aggs[i]['printer_per_day']['buckets'][j]['key']).getDay() == new Date().getDay()) {
                                for (var k in aggs[i]['printer_per_day']['buckets'][j]['print_per_hour']['buckets']) {
                                    var day = new Date(aggs[i]['printer_per_day']['buckets'][j]['print_per_hour']['buckets'][k]['key']);
                                    var time = day.getHours();

                                    for (var c = 0; c < hours.length; c++) {
                                        if (time >= hours[c] && time < (hours[c]) + 1.00) {

                                            map[c] += aggs[i]['printer_per_day']['buckets'][j]['print_per_hour']['buckets'][k]['doc_count'];
                                        }


                                    }

                                }


                            }
                        }

                        tmap[aggs[i].key] = map;

                    }

                    for(var j in tmap[aggs[i].key]){

                        if(tmap[aggs[i].key][j] != 0) {

                            tmap[aggs[i].key][j] = (tmap[aggs[i].key][j] / 4);
                        }
                    }

                    sorted.push({printerId: aggs[i].key, data: tmap[aggs[i].key]})

                }


                if (sorted.length > 0) {
                    var res = [];
                    for (var i in printers) {
                        for (var j in sorted) {

                            if (printers[i].printerId == sorted[j].printerId) {
                                res.push({printerId: printers[i].printerId, data: sorted[j].data})

                            }

                        }
                    }

                    resolve(res);
                }
                else {
                    reject(Error("It broke"));
                }
            });


    });

};

/*
 * getPrintersHourly is a function used to make the table consisting of printers on the Hourly tab, on the right.
 * The table shows either if the printer prints less or prints more today, than usual.
 * In this function, we calculate the average, and then calculate variance and standard deviation to avoid
 * smaller printers dominating the table of printers, because if a small printer on average prints 1 pr. day, then printing 2
 * can cause a growth of 100%.
 * So before a printer can be on the table, it must have printjobs*2 of its standard deviation.
 * Here we use Elasticsearch to find all printjobs from last month till today, so we can calculate all the
 * things mentioned above.
 *
 * getPrintersHourly returns printers, than is used in methods lke searchHourlyAvg and searchHourlyCharts, so they
 * can return the right printers to show on the charts.
 *
 * @param element - Takes a collection. Elasticsearch is then used to search through the collection.
 * @param printers - Takes an array to get the correct hostId, locationId etc. from a printer.
 * @param threshold - A printer needs a certain amount of printjobs from yesterday, before it can be part of the table.
 * this is to avoid smaller printers to dominate the table.
 */

service.getPrintersHourly = function (element,printers,threshold) {
    var other = [];
    var docSize = 0;
    var printer = [];
    var avg = 0;
    var sum = 0;
    var sum2 = 0;
    var countSize = 0;
    var newDate = new Date().getTime();
    var oldDate = new Date().setMonth(new Date().getMonth() - 1);

    return new Promise(function(resolve, reject) {
        element.search(
            {"match_all": {}},
            {
                "aggs": {
                    "createdAt-range" :{
                        "range": {
                            "field" : "createdAt",
                            "ranges" : [{"from": oldDate, 'to': newDate}]
                        },
                        "aggs" : {
                            "group_by_printerId": {
                                "terms": {
                                    "field": "printerId"
                                    ,"size": 214748364,
                                    "min_doc_count" : 0
                                },
                                "aggs": {
                                    "print_hour": {
                                        "date_histogram": {
                                            "field": "createdAt",
                                            "interval": "1h"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            function(err, results) {
                var aggs = results['aggregations']['createdAt-range']['buckets'][0]['group_by_printerId']['buckets'];

                for(var i in aggs){


                    for (var j in aggs[i]['print_hour']['buckets']) {

                        if (new Date(aggs[i]['print_hour']['buckets'][j]['key']).getWeek() != new Date().getWeek()) {

                            if(new Date(aggs[i]['print_hour']['buckets'][j]['key']).getWeek() == new Date().getWeek() - 1) {
                                docSize += aggs[i]['print_hour']['buckets'][j]['doc_count'];
                            }

                            if (new Date(aggs[i]['print_hour']['buckets'][j]['key']).getDay() == new Date().getDay()) {

                                if(new Date(aggs[i]['print_hour']['buckets'][j]['key']).getHours() < new Date().getHours() &&
                                    new Date(aggs[i]['print_hour']['buckets'][j]['key']).getHours() >= new Date().getHours() - 1) {
                                    sum2 += aggs[i]['print_hour']['buckets'][j]['doc_count'];

                                }
                            }

                        }


                        if (new Date(aggs[i]['print_hour']['buckets'][j]['key']).getWeek() === new Date().getWeek()) {

                            if (new Date(aggs[i]['print_hour']['buckets'][j]['key']).getDay() === new Date().getDay()) {

                                if (new Date(aggs[i]['print_hour']['buckets'][j]['key']).getHours() < new Date().getHours() &&
                                    new Date(aggs[i]['print_hour']['buckets'][j]['key']).getHours() >= new Date().getHours() - 1) {
                                    sum += aggs[i]['print_hour']['buckets'][j]['doc_count'];

                                }

                            }
                        }


                    }

                    var sum3  = (sum2 / 4);

                    if(sum != 0 && sum3 != 0) {
                        avg = ((sum - sum3) / sum3) * 100;
                        if (docSize > threshold) {
                            printer.push({printerId: aggs[i].key, avg: avg.toFixed(2)});
                        }
                    } else if(sum == 0 && sum3 != 0){
                        avg = ((sum - sum3) / sum3) * 100;
                        if (docSize > threshold) {

                            printer.push({printerId: aggs[i].key, avg: avg});

                        }
                    }
                    sum = 0;
                    sum2 = 0;
                    docSize = 0;

                }

                printer.sort(function (a,b) {
                    if(b.avg < 0 && a.avg < 0) {
                        return a.avg - b.avg;
                    } else {
                        return b.avg - a.avg;
                    }
                });


                for(var i in printer) {


                    if (printer[i].avg > 0) {
                        if (other.length <= 24) {
                            other.push({printerId: printer[i].printerId, avg: printer[i].avg})
                            countSize++;
                        }
                    } else {
                        if (other.length <= 24 + countSize && printer[i].avg < 0) {
                            other.push({printerId: printer[i].printerId, avg: printer[i].avg})
                        }
                    }

                }

                var m = [];
                var matches = [];

                if (other.length > 0) {
                    m = printers;

                    for(var j in other) {
                        for(var n in m){
                            if (m[n].printerId === other[j].printerId) {
                                matches.push({printerId: m[n].printerId, hostId: m[n].hostId, locationId: m[n].locationId, avg: other[j].avg});
                            }
                        }
                    }
                    resolve(matches);
                }
                else {
                    reject(Error("It broke"));
                }
            });


    });


};

module.exports = service;