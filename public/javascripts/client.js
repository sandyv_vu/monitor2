/**
 * Created by Neels1 on 15/08/2017.
 */

var updateChart = function (chart, arr1, arr2, index) {

    chart.data.datasets[0].data = arr1[index].data;
    chart.data.datasets[1].data = arr2[index].data;

    chart.update();
}

var sortWeekdays = function () {
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var weekdays = [];
    var start = 0;
    var d = new Date();
    for(var i = 0; i < days.length; i++){

        if(d.getDay() != 0) {
            if (i > d.getDay() - 1) {
                weekdays.push(days[i]);
            }
            if (i == d.getDay() - 1) {
                start = i;
            }
        } else {
            if (i == 6) {
                start = i;
            }
        }
    }


    for(var i = 0; i <= start; i++){
        weekdays.push(days[i]);
    }
    return weekdays;
}


