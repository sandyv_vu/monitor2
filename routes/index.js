var express = require('express');
var router = express.Router();
var Printjob = require('../models/printjob');
var PrintersLocations = require('../models/printers_location');
var Service = require('../src/service');

/*
 * This function calls our synchronizations.
 * There is also a interval that keeps calling the sync functions,
 * to get new data after a certain amout of time..
 *
 */


new Promise(function (resolve, reject) {
    setInterval(function () {
        Service.startSyncLocations(PrintersLocations);
    }, 86400000);


    setInterval(function () {
        Service.startSyncPrintjobs(Printjob);
    }, 600000);
    return resolve();

});

/*
 * router.get is the method that renders the page. It starts with creating a lot of different arrays.
 * We need a positive and a negative for each table and chart.
 * It calls all our search functions from service and puts the data into our arrays.
 * It looks at the table arrays and takes the ones with positive values and puts in one array
 * and the negative in another. it then calls our filterArrays function,
 * ex. the table array for daily and the chart array for daily, which sorts the chart
 * daily array to look like the table array.
 * This is done so the table and the charts match each other.
 * It then calls the res.render to the printjob_drops.hbs file and send all our data to the client.
 */

router.get('/', function (req, res) {

    var printersDailyNegative = [];
    var printersDailyPositive = [];
    var printersWeeklyNegative = [];
    var printersWeeklyPositive = [];
    var printersHourlyNegative = [];
    var printersHourlyPositive = [];

    var printers = Service.searchAll(PrintersLocations);
    printers.then(function (printers) {

        var printersDaily = Service.getPrintersDaily(Printjob, printers);
        var printersWeekly = Service.getPrintersWeekly(Printjob, printers);
        var printersHourly = Service.getPrintersHourly(Printjob, printers, 20);


        printersHourly.then(function (printersHourly) {

            printersDaily.then(function (printersDaily) {

                printersWeekly.then(function (printersWeekly) {

                    var daily = Service.searchDailyCharts(Printjob, printersDaily);
                    var dailyAvg = Service.searchDailyAvg(Printjob, printersDaily);
                    var weekly = Service.searchWeeklyCharts(Printjob, printersWeekly);
                    var weeklyAvg = Service.searchWeeklyAvg(Printjob, printersWeekly);
                    var hourly = Service.searchHourlyCharts(Printjob, printersHourly);
                    var hourlyAvg = Service.searchHourlyAvg(Printjob, printersHourly);



                    for(var i in printersWeekly){
                        if(printersWeekly[i].growth > 0){
                            printersWeeklyPositive.push({printerId: printersWeekly[i].printerId, locationId: printersWeekly[i].locationId, hostId: printersWeekly[i].hostId, growth: printersWeekly[i].growth, avg: printersWeekly[i].avg});

                        } else {
                            printersWeeklyNegative.push({printerId: printersWeekly[i].printerId, locationId: printersWeekly[i].locationId, hostId: printersWeekly[i].hostId, growth: printersWeekly[i].growth, avg: printersWeekly[i].avg});

                        }
                    }

                    for(var i in printersDaily){
                        if(printersDaily[i].growth > 0){
                            printersDailyPositive.push({printerId: printersDaily[i].printerId, locationId: printersDaily[i].locationId, hostId: printersDaily[i].hostId, growth: printersDaily[i].growth, avg: printersDaily[i].avg});
                        } else {
                            printersDailyNegative.push({printerId: printersDaily[i].printerId, locationId: printersDaily[i].locationId, hostId: printersDaily[i].hostId, growth: printersDaily[i].growth, avg: printersDaily[i].avg});
                        }
                    }
                    for(var i in printersHourly){
                        if(printersHourly[i].avg > 0){
                            printersHourlyPositive.push({printerId: printersHourly[i].printerId, locationId: printersHourly[i].locationId, hostId: printersHourly[i].hostId, avg: printersHourly[i].avg});
                        } else {
                            printersHourlyNegative.push({printerId: printersHourly[i].printerId, locationId: printersHourly[i].locationId, hostId: printersHourly[i].hostId, avg: printersHourly[i].avg});
                        }
                    }


                    dailyAvg.then(function (dailyAvg) {
                        daily.then(function (daily) {
                            weekly.then(function (weekly) {
                                weeklyAvg.then(function (weeklyAvg) {
                                    hourly.then(function (hourly) {
                                        hourlyAvg.then(function (hourlyAvg) {


                                            res.render('printjob_drops', {Daily: daily, DailyAvg: dailyAvg,  PrintersDailyPositive: printersDailyPositive, PrintersDailyNegative: printersDailyNegative, Weekly: weekly,
                                                WeeklyAvg: weeklyAvg, PrintersWeeklyPositive: printersWeeklyPositive, PrintersWeeklyNegative: printersWeeklyNegative, PrintersHourlyNegative: printersHourlyNegative,
                                                PrintersHourlyPositive: printersHourlyPositive, PrintersHourlyPositive: printersHourlyPositive, PrintersHourlyNegative: printersHourlyNegative, Hourly: hourly, HourlyAvg: hourlyAvg});


                                        });
                                    });
                                });
                            });
                        });

                    });
                });

            });
        });

    });

});

module.exports = router;



