var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var request = require('request');

var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('--Fjernet grundet fortrolighed', { useMongoClient: true });


var app = express();
app.set('port', (process.env.PORT || 8080));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
var index = require('./routes/index');
app.use('/', index);

var http = require("http");


var data = {
    "query": {
        "range": {
            "createdAt": {"lte": new Date().getTime() - (604800000*6)}
        }
    }
};

var deletePrintjobs = {method: 'POST', body: data, url: 'fjernet grundet fortrolighed', json: true};
request(deletePrintjobs, function (err, resp, body) {
    console.log(body);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
});



app.listen(8080, function () {
    console.log("Listening on port", 8080);
});

module.exports = app;
