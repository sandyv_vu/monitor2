/**
 * Created by Neels1 on 11/08/2017.
 */
var mongoose     = require('mongoose')
    , mongoosastic = require('mongoosastic')
    , Schema       = mongoose.Schema;

var printjob = new Schema({
        createdAt: Number,
        printerId: String,
        payment: {"gateway": {type: String}, "paytype":{type: String}}
    },
    {collection: 'printjob_registry-printjobs'}
);


printjob.plugin(mongoosastic, {
    hosts: [
        {
            host: 'elastic.bloch-monitor.princh.com',
            auth: 'fjernet grundet fortrolighed',
            protocol: 'http',
            port: 80
        },
    ]
});



module.exports = mongoose.model('printjob', printjob);



















