/**
 * Created by Neels1 on 17/08/2017.
 */

var mongoose     = require('mongoose')
    , mongoosastic = require('mongoosastic')
    , Schema       = mongoose.Schema;

var printers_location = new Schema({
        printerId: String,
        hostId: String,
        locationId: String

    },
    {collection: 'printers_with_location'}
);

printers_location.plugin(mongoosastic, {
    hosts: [
        {
            host: 'elastic.bloch-monitor.princh.com',
            auth: 'fjernet grundet fortrolighed',
            protocol: 'http',
            port: 80
        },
    ]
});


module.exports = mongoose.model('printers_with_location', printers_location);
